﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalTeleporter : MonoBehaviour {

    public Transform player;
    public Transform receiver;

    private bool playerIsOverlapping = false;

    private float overlapDelay = 0;

	// Update is called once per frame
	void Update ()
    {
        if (overlapDelay > 0) overlapDelay++;
        if (overlapDelay > 2) overlapDelay = 0;


        if (playerIsOverlapping && overlapDelay <= 0)
        {
            overlapDelay = 5;
            Vector3 portalToPlayer = player.position - transform.position;
            float dotProduct = Vector3.Dot(transform.up, portalToPlayer);

            if (dotProduct < 0f)
            {
                float rotationDiff = -Quaternion.Angle(transform.rotation, receiver.rotation);
                rotationDiff += 180;
                player.Rotate(Vector3.up, rotationDiff);

                Vector3 positionOffset = Quaternion.Euler(0f, rotationDiff, 0f) * portalToPlayer;
                player.position = receiver.position + positionOffset;

                playerIsOverlapping = false;
            }
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player" && overlapDelay <= 0) playerIsOverlapping = true;

    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            playerIsOverlapping = false;
        }
    }
}
