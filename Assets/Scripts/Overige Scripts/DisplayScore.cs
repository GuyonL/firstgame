﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayScore : MonoBehaviour {

    public Text textScore;
    public Text textHighscore;

    private void OnEnable()
    {
        Debug.Log(PlayerPrefs.GetInt("highscore"));
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        textScore.text = PlayerPrefs.GetInt("score").ToString();
        textHighscore.text = PlayerPrefs.GetInt("highscore", PlayerPrefs.GetInt("score")).ToString();
    }
}
