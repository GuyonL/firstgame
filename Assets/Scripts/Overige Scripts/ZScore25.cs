﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ZScore25 : MonoBehaviour
{

    // Use this for initialization
    public void DeductPoints(int DamageAmmount)
    {
        GlobalScore.CurrentScore += 25;
        PlayerPrefs.SetInt("score", GlobalScore.CurrentScore);

    }
}