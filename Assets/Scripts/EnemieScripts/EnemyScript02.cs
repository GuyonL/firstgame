﻿using UnityEngine;
using System.Collections;

public class EnemyScript02 : MonoBehaviour
{
    public int DamageAmount = 5;
    public float TargetDistance;
    public float AllowedRange = 15.0f;
    public bool deadAnimationPlaying;
    public AnimationClip deadAnimation;

    private Animation a;

    // Update is called once per frame
    public int EnemyHealth = 10;

    private bool isDead = false;

    void Awake()
    {
        a = transform.GetChild(0).GetComponent<Animation>();
    }

    void DeductPoints(int DamageAmount)
    {
        EnemyHealth -= DamageAmount;
    }

    void Update()
    {
        if (!isAlive() && !deadAnimationPlaying && !isDead)
        {
            isDead = true;
            StartCoroutine(die());
        }
    }

    public bool isAlive()
    {
        return EnemyHealth > 0;
    }

    public IEnumerator die()
    {
        deadAnimationPlaying = true;
        a.Stop();
        a.clip = deadAnimation;
        a.Play();
        yield return new WaitForSeconds(deadAnimation.length);
        Destroy(gameObject);
    }
}