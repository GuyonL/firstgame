﻿using UnityEngine;
using System.Collections;

public class EnemyScript : MonoBehaviour
{
    public int DamageAmount = 5;
    public float TargetDistance;
    public float AllowedRange = 15.0f;
    public float Timer;

    // Update is called once per frame
    public int EnemyHealth = 10;

    void DeductPoints(int DamageAmount)
    {
        EnemyHealth -= DamageAmount;
    }

    void Update()
    {
        if (EnemyHealth <= 0)
        {
            GetComponent<Animation>().Play("Death");

            Timer++;
            if (Timer == 10) {
                Destroy(gameObject);
            }

        }
    }
}