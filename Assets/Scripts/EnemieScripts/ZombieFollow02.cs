﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieFollow02 : MonoBehaviour
{

    public GameObject ThePlayer;
    public float TargetDistance;
    public float AllowedRange = 10;
    public GameObject TheEnemy;
    public float EnemySpeed;
    public int AttackTrigger;
    public float attackRange;
    public RaycastHit Shot;
    public EnemyScript02 enemyScript;

    [SerializeField]
    private Rigidbody body;

    private void Update()
    {
        if (enemyScript.isAlive())
        {
            // get normal zombie is standing on
            Vector3 normal = Vector3.up;
            RaycastHit hit;
            if (Physics.Raycast(transform.position, -transform.up, out hit))
                normal = hit.normal;

            // limmit lookat on y axis
            Vector3 lookatVector = ThePlayer.transform.position;
            lookatVector.y = transform.position.y;
            transform.LookAt(lookatVector);
            if (inAttackRange(0) == 1)
            {
                EnemySpeed = 0;
                if (!TheEnemy.GetComponent<Animation>().IsPlaying("attack(4)"))
                {
                    TheEnemy.GetComponent<Animation>().Play("attack(4)");
                    ThePlayer.GetComponent<vp_FPPlayerDamageHandler>().Damage(3);
                }
                
                //transform.position = Vector3.MoveTowards(transform.position, ThePlayer.transform.position, EnemySpeed);
            }
            else if(getDistanceToPlayer() > attackRange * 1.4f)
            {
                if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out Shot))
                {
                    TargetDistance = Shot.distance;
                    if (TargetDistance < AllowedRange)
                    {
                        EnemySpeed = 5;//0.085f;
                            if (!TheEnemy.GetComponent<Animation>().IsPlaying("walk")) TheEnemy.GetComponent<Animation>().Play("walk");
                            //transform.position = Vector3.MoveTowards(transform.position, ThePlayer.transform.position, EnemySpeed);
                            
                    }
                }
            } else
            {
                
                //transform.position = Vector3.MoveTowards(transform.position, ThePlayer.transform.position, EnemySpeed);
            }
            // move player based on terrein normal
            Vector3 movement = (ThePlayer.transform.position - transform.position).normalized * EnemySpeed;
            movement.y = 0;
            body.velocity = Vector3.ProjectOnPlane(movement, normal);
        }
    }

    public int inAttackRange(float f)
    {
        float dist = Vector3.Distance(transform.position, ThePlayer.transform.position);
        return (dist < attackRange + f) ? 1 : 0;
    }

    public float getDistanceToPlayer()
    {
        return Vector3.Distance(transform.position, ThePlayer.transform.position);
    }

    private void OnTriggerEnter()
    {
        AttackTrigger = 1;
    }

    private void OnTriggerExit()
    {
        AttackTrigger = 0;
    }

}